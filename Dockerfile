ARG TAG=latest
FROM mariadb:${TAG}

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update \
    && apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" upgrade \
    && apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" dist-upgrade \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/*

COPY multi-db.sh /docker-entrypoint-initdb.d/
