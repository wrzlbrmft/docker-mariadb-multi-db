# docker-mariadb-multi-db

MariaDB with multiple databases in Docker.

Based on https://hub.docker.com/_/mariadb .

```
docker pull wrzlbrmft/mariadb-multi-db:<version>
```

This will create two databases - `foo` and `bar`:

```
docker run -p 3306:3306 -e MARIADB_ROOT_PASSWORD=r00t -e MARIADB_DATABASES="foo bar" wrzlbrmft/mariadb-multi-db:latest
```

Next, connect to `localhost:3306` with username `root` and password `r00t`.

See also:

  * https://mariadb.org/
  * https://hub.docker.com/r/wrzlbrmft/mariadb-multi-db/tags

## Kubernetes (GKE)

```
gcloud compute disks create mariadb \
--size=10G \
--type=pd-ssd \
--zone=us-east4-a
```

```
helm install --set staticPersistentVolume=true mariadb docker/helm/mariadb-multi-db
```

## License

The content of this repository is distributed under the terms of the
[GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html).
