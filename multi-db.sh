file_env MARIADB_DATABASES
if [ -n "$MARIADB_DATABASES" ]; then
    for MARIADB_DATABASE in $MARIADB_DATABASES; do
        mysql_note "Creating database ${MARIADB_DATABASE}"
        docker_process_sql --database=mysql <<<"CREATE DATABASE IF NOT EXISTS \`$MARIADB_DATABASE\` ;"

        if [ -n "$MARIADB_USER" ] && [ -n "$MARIADB_PASSWORD" ]; then
            mysql_note "Giving user ${MARIADB_USER} access to schema ${MARIADB_DATABASE}"
            docker_process_sql --database=mysql <<<"GRANT ALL ON \`${MARIADB_DATABASE//_/\\_}\`.* TO '$MARIADB_USER'@'%' ;"
        fi
    done
fi
